package mongte

import (
	"errors"
	"math"
	"time"

	"gitlab.com/my0sot1s/helper"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ObjectHex wrap ObjectIdHex
var oh = bson.ObjectIdHex

//DbConnector wrap any connector to db
type DbConnector struct {
	mgodb  *mgo.Database
	host   string
	user   string
	pw     string
	dbname string
}

// InitMongo is initial a new connection
func (c *DbConnector) InitMongo(host, user, pw, dbname string) {
	// session, err := mgo.Dial(url)
	c.host, c.user, c.pw, c.dbname = host, user, pw, dbname
	c.retryConnect(3)
	helper.Log("+ DB CONNECTED DBNAME : ", dbname)
}

func (c *DbConnector) retryConnect(num int) {
	for i := 0; i < num; i++ {
		if c.openConnection() != nil {
			continue
		}
		return
	}
}

func (c *DbConnector) openConnection() error {
	session, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{c.host},
		Timeout:  20 * time.Second,
		Database: c.dbname,
		Username: c.user,
		Password: c.pw,
	})
	if err != nil {
		return err
	}
	session.SetMode(mgo.Monotonic, true)
	c.mgodb = session.DB(c.dbname)
	return nil
}

//----------------- end of connection config -----------------

// Insert insert Single ???
func (c *DbConnector) insertDb(coll string, data helper.M) (helper.M, error) {
	bsonID := bson.NewObjectId()
	data["_id"] = bsonID
	if err := c.mgodb.C(coll).Insert(data); err != nil {
		return nil, err
	}
	return c.readByID(coll, bsonID.Hex())
}

// only id no have _id
// Update single value
func (c *DbConnector) updateDb(coll string, selector helper.M, updater helper.M) (helper.M, error) {
	if selector["id"] == nil {
		return nil, errors.New("selector nil id")
	}
	pureID := helper.PIf2Str(selector["id"])
	selector["_id"] = bson.ObjectIdHex(pureID)
	delete(selector, "id")
	// update
	if err := c.mgodb.C(coll).Update(selector, helper.M{"$set": updater}); err != nil {
		return nil, err
	}
	// check resuilt
	return c.readByID(coll, pureID)
}

// Delete Single value
func (c *DbConnector) deleteDb(coll string, selector helper.M) error {
	if selector["id"] == nil {
		return errors.New("selector nil id")
	}
	selector["_id"] = bson.ObjectIdHex(helper.PIf2Str(selector["id"]))
	delete(selector, "_id")
	return c.mgodb.C(coll).Remove(selector)
}

// readBy is a func for read Db
func (c *DbConnector) readBy(coll, anchor, sortBy string, limit int, conditions helper.M) ([]helper.M, error) {
	result, query := make([]helper.M, 0), helper.M{}
	if anchor != "" {
		if limit < 0 {
			query = helper.M{"_id": helper.M{"$lt": oh(anchor)}}
		} else {
			query = helper.M{"_id": helper.M{"$gt": oh(anchor)}}
		}
	}
	if conditions != nil {
		for k, c := range conditions {
			query[k] = c
		}
	}
	limit = int(math.Abs(float64(limit)))
	if err := c.mgodb.C(coll).Find(query).Limit(limit).Sort(sortBy).All(&result); err != nil {
		return nil, err
	}
	if len(result) > 0 {
		for _, r := range result {
			r = castRaw2Real(r)
		}
	}
	return result, nil
}

// Read read all by condition
func (c *DbConnector) read(coll, anchor, sortBy string, limit int, conditions helper.M) ([]helper.M, error) {
	data, err := c.readBy(coll, anchor, sortBy, limit, conditions)
	if err != nil {
		return nil, err
	}
	for _, v := range data {
		v["id"] = v["_id"]
		delete(v, "_id")
	}
	return data, nil
}

// ReadByID get one
func (c *DbConnector) readByID(coll, id string) (helper.M, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, helper.CreateError("Is not object hex")
	}
	var result helper.M
	if err := c.mgodb.C(coll).FindId(bson.ObjectIdHex(id)).One(&result); err != nil {
		return nil, err
	}
	result["id"] = result["_id"]
	delete(result, "_id")
	return result, nil
}

//ReadByIDs get list items by ids
func (c *DbConnector) readByIDs(coll string, ids []string) ([]helper.M, error) {
	// make condition
	slideIds, cond, result := make([]bson.ObjectId, 0), make(helper.M), make([]helper.M, 0)
	for _, id := range ids {
		if !bson.IsObjectIdHex(id) {
			return nil, helper.CreateError("Some Id not object hex")
		}
		slideIds = append(slideIds, bson.ObjectIdHex(id))
	}
	cond["_id"] = helper.M{"$in": slideIds}
	if err := c.mgodb.C(coll).Find(cond).All(&result); err != nil {
		return nil, err
	}
	if len(result) > 0 {
		for _, r := range result {
			r = castRaw2Real(r)
		}
	}
	return result, nil
}
