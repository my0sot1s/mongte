package mongte

import (
	"gitlab.com/my0sot1s/helper"
)

func castRaw2Real(m helper.M) helper.M {
	delete(m, "__v")
	return m
}

/*********************** helper for request ******************/

// GetRaw get from db
func (c *DbConnector) GetRaw(res interface{}, rq helper.MS, options helper.M) error {
	name := helper.PIf2Str(options["name"])
	delete(options, "name")
	limit, _ := helper.PIf2Int(rq["limit"])
	raw, err := c.read(name, rq["anchor"], rq["order_by"], limit, options)
	if err != nil {
		return err
	}
	return helper.ConveterM2I(raw, &res)
}

// GetOneRaw get 1 record from db
func (c *DbConnector) GetOneRaw(res interface{}, rq helper.MS, options helper.M) error {
	raw, err := c.readByID(helper.PIf2Str(options["name"]), rq["id"])
	if err != nil {
		return err
	}
	return helper.ConveterM2I(raw, &res)
}

// GetByIDs get 1 record from db
func (c *DbConnector) GetByIDs(res interface{}, ids []string, options helper.M) error {
	// ids := make([]string, 0)
	// helper.ConveterM2I(rq["ids"], &ids)
	raw, err := c.readByIDs(helper.PIf2Str(options["name"]), ids)
	if err != nil {
		return err
	}
	return helper.ConveterM2I(raw, &res)
}

// InsertRaw insert from db
func (c *DbConnector) InsertRaw(res interface{}, data helper.M, options helper.M) error {
	raw, err := c.insertDb(helper.PIf2Str(options["name"]), data)
	if err != nil {
		return err
	}
	return helper.ConveterM2I(raw, &res)
}

// UpdateRaw update from db
func (c *DbConnector) UpdateRaw(res interface{}, selector, updater helper.M, options helper.M) error {
	raw, err := c.updateDb(helper.PIf2Str(options["name"]), selector, updater)
	if err != nil {
		return err
	}
	return helper.ConveterM2I(raw, &res)
}

// DeleteRaw update from db
func (c *DbConnector) DeleteRaw(selector helper.M, options helper.M) error {
	return c.deleteDb(helper.PIf2Str(options["name"]), selector)
}
